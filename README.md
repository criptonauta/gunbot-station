###This project is currently closed and archived. ###

![criptonauta-horizontal.png](https://bitbucket.org/repo/da6GyXo/images/2728898920-criptonauta-horizontal.png)

# Welcome to Criptonauta’s Gunbot Station! #

This is a work in development. Your contributions will be very useful for make the system better! Please share your comments in telegram [@Criptonauta](https://t.me/Criptonauta) or [PM me in Gunthy forum](https://gunthy.org/index.php?action=pm;sa=send;u=16).


### DISCLAIMER: Although the system is partially based in open-source code, it also contains proprietary code. Besides, it took me a lot of effort and hours to make this format so it can be easily used by anyone. So please do not share it, sell it or distribute it without consulting me. ###

### Introduction ###
This system started to be developed when I changed from Windows to Linux Gunbot, still in the beginning of the 2.x versions. As a first user in Linux and finding the tutorials in Gunthy a bit confusing, I decided to go my own way and discovered Tmux, a terminal multiplexer that is being used as the framework for my GB System.

Today, it’s integrated with several features that I consider useful when piloting Gunbot. Some more will be available as time comes. 

## Installation and how to login ##

### A - LINUX ###
When in Linux, access is very straght forward: Open a terminal and SSH to your VPS.

### B - WINDOWS ###
When using Windows, the tool of choice to login in the system is Putty. Let's set it up:

1 – In Session tab, insert your VPS IP address in ‘Host Name’ field

![putty1.png](https://bitbucket.org/repo/da6GyXo/images/3506186801-putty1.png)

2 – Go to Translation section. From there, in ‘Remote Character Set’ choose one of the options: *UTF-8* or *Use Font Encoding* (the last option in the list and the one I generally use).

![putty2.png](https://bitbucket.org/repo/da6GyXo/images/4140295826-putty2.png)

3 – Go back to Session section. This time, type the name of your session in ‘Saved Sessions’ and click Save. Now your config will be stored for future uses.

4 – Double click in your profile name, to access the server:

In Contabo, username is usually ‘root’

![contabo.png](https://bitbucket.org/repo/da6GyXo/images/1745830985-contabo.png)

The terminal will ask for your password. Copy it and paste it with Ctrl + Right Click. Note that the password characters will not be displayed.

If everything is correct, you will receive a welcome message:

![login.png](https://bitbucket.org/repo/da6GyXo/images/1109307235-login.png =400x)


## Accessing and knowing your Gunbot Station ##

Once logged in, type **gunbot** to access the system. A new window will appear.

![screen1.png](https://bitbucket.org/repo/da6GyXo/images/1178571429-screen1.png =300x)

## Layout ##
In the image above, we can see the screen is divided in several ways. We are inside a *session* called **gunbot**. The top menu shows different tabs or *windows*. Each window can contain different *panes*:

![layout.png](https://bitbucket.org/repo/da6GyXo/images/2014907292-layout.png =400x)

## Basic Commands ## 

Moving around different windows or panes can be also be accomplished with the mouse. However, typed commands allow more malleability when creating, managing or monitoring pairs.

### Prefix Operator ###
Before going further, it’s important to understand how the prefix operator works. My Gunbot Station is based in Tmux and it has a customizable prefix sequence (using my file, it’s **ctrl + a**), and you must type this before typing the next key.

For example, when creating a new window , it’s prefix + c. This means you first hold down the ctrl key, then the a key, then you release both (just like copying and pasting). Now press the c key. By pressing the prefix sequence first, you’re entering command mode.

### Moving Around ###
You can use your mouse all the time. Some limitations still exist, like to copy and paste data, and they will be addressed in a near future.

Besides the mouse controlling, to move around between panes and windows, here are some commands:

To *move to the next window*, type Prefix + n or **Ctrl+a** and then press **n**
To *move to a previous window*, type Prefix + n or **Ctrl+a** and then press **p**
To *move to the any window*, type Prefix + its corresponding number. Ex: **Prefix + 0** to go to System window
To *visualize all the windows* (and thus the pairs running), type Prefix + w or **Ctrl+a** and then press **w**. From there, to go to a windows, you can left click or using keyboard arrows to select a window, then press Enter. You also can just type the window number.

![ctrl+a_w.png](https://bitbucket.org/repo/da6GyXo/images/2428165583-ctrl+a_w.png)
 
To *move around panes*, just click the desired one or **Prefix + UP, DOWN, LEFT or RIGHT** arrows. A *right click* in a pane will **highlight** it.
 
All panes can be zoomed out by using Ctrl+A z. Panes can be resized with mouse by dragging borders.

## Creating Windows & Panes ##
Tmux supports several windows as well as splitting a window into any number of panes (both horizontally and vertically). For your convenience, there are pre-made windows that fits up to 32 pairs.

However, as time goes, you might need new ones, or to rename the existing ones.

To *create a new window*, type **prefix + c** (prefix being ctrl + a).

Once in your new tab, you can *set a name for the tab* using **prefix + ,** (just enter the new name a press enter). Naming tabs is the key to staying organized!

To *create a vertical pane*, type **prefix + |** (character above backslash)
To *create a horizontal pane*, type **prefix + -** (minus) 
To *close a tab with only one pane*, type **prefix + x**.

In order to *make easier the process of creating a new formatted windows*, you can use the following sequence of commands:

**Prefix + A** (shift a)

It will generate a new window to hold six pairs.

![layout](https://i.gyazo.com/fa09f75a4b5a644c626832b396c3d2b9.png)

OBS: According to the size of your screen, 8 panes can be used. The default number assures all the needed info about a pair will be displayed in the pane.

## Gunbot Commands ##

### Creating and Staring Pairs ###

####Gunbot 3.0.2####
To create new pairs, use the command **pairs** and the pair generator will appear:

![zeng.png](https://bitbucket.org/repo/da6GyXo/images/21373239-zeng.png =400x)

Just follow the instructions, typing the the number of the chosen alternative.
![zeng1.png](https://bitbucket.org/repo/da6GyXo/images/3988169278-zeng1.png =200x) | ![zeng2.png](https://bitbucket.org/repo/da6GyXo/images/2996271080-zeng2.png =200x)

The default values for each strategy can be further customized by editing the file *poloniex-config.js*, located inside Gunbot folder.

After inserting the desired parameters (or just pressing Enter to use default values) you can revise the settings. If everything ok, type Y and the config file for those pairs will be created. If something is not ok, type N and restart the process.
Right now, the auto pair is not starting the pairs in a desirable way so it is currently unavailable. Type n  to star the pairs manually.

Now, click on each pane, type the command to start the pair and press Enter. Pairs will be started.
![zeng3.png](https://bitbucket.org/repo/da6GyXo/images/344927776-zeng3.png =400x)

For *BTC pairs*, the command is **polo_COIN**. Ex: polo_ETH
For *ETH pairs*, the command is **polo_ETH_COIN**. Ex: polo_ETH_REP
For *XMR pairs*, the command is **polo_XMR_COIN**. Ex: polo_XMR_ZEC
For *USDT pairs*, the command is **polo_USDT_COIN**. Ex: polo_USDT_BTC
For *Bittrex*, just replace polo by bit. Ex: **bit_ETH**

TIP: After creating and starting all the pairs, *rename the window* with **Ctrl+a ,** and name it accordingly.


####Gunbot 2.0.4####
For *BTC pairs*, the command is **BTC_COIN**. Ex: BTC_ETH
For *ETH pairs*, the command is **ETH_COIN**. Ex: ETH_REP
For *XMR pairs*, the command is **XMR_COIN**. Ex: XMR_ZEC
For *USDT pairs*, the command is **USDT_COIN**. Ex: USDT_BTC


## Tools ##

For your convenience, Cripto’s Gunbot System offers the following tools to help you with Poloniex and Bittrex related data. Below, their description and commands to run them.

Please observe that accuracy of some data depends on Polo accuracy. So, if there are many 422 errors, some information can be wrong (like in deposits) or make the program to crash. The first reading of the program, though, is always accurate. If the program crashes, just run it again.

HARDWARE MONITOR | Command: **htop**
---------------- | ----------------
![htop.png](https://bitbucket.org/repo/da6GyXo/images/869501293-htop.png) | Displays useful information about CPU, memory and running processes. If some process is stucked or consuming too much resource (like the bug in Gunbot Monitor when display the profit with -P), just click in the process, then press F9 to kill it.

VOLUME ANALYZER | Command: **getBTC**, **getETH**, **getXMR**, **getUSDT**
---------------- -------------------------------------------------------
![![Selection_428.png](https://bitbucket.org/repo/da6GyXo/images/942736025-Selection_428.png)](https://bitbucket.org/repo/da6GyXo/images/3995868418-Selection_428_.png) | Display the coins with highest volume change during a period of time ranging from 1 week to 1 hour. Still unavailable for Bittrex.

TRADE HISTORY | Command: **trades**
-------------- --------------------
![Selection_430.png](https://bitbucket.org/repo/da6GyXo/images/1525971102-Selection_430.png) | According to user input, display a number of last trades after x seconds. It also displays original balance, current balance and profit estimation.
--------------|-------------------
![Selection_429.png](https://bitbucket.org/repo/da6GyXo/images/876707929-Selection_429.png) | Some information may appear erroneously due to Poloniex 422 errors. The first reading is always accurate.

Note: If you are using Polo and Trex, commands are:
Polo: **trades**
Bittrex: **btrades**

If Bittrex only, command is trades

KNOW YOUR FEES | Command: **fees**
---------------|--------------
![Selection_431.png](https://bitbucket.org/repo/da6GyXo/images/1769280337-Selection_431.png) | Lists how much fee you have paid for the coins you have traded, showing the total amount in BTC.

PROFITS RESUMED | Command: **overwiew**
----------------|------------------
![profits.png](https://bitbucket.org/repo/da6GyXo/images/2677715847-profits.png) | Displays a resume of your profit with a comment about the gain obtained.

PROFITS IN DETAILS | Command: **profits**
-------------------|---------------------
![IMAGE](https://bitbucket.org/repo/da6GyXo/images/48928138-Selection_432.png) | Displays a detailed view of profit or loss in each coin traded.

GUNBOT MONITOR (by Beer-Koin) | Command: **monitor**
------------------------------|---------------------
![IMAGE](https://bitbucket.org/repo/da6GyXo/images/3903928067-gmon.png) | Displays useful information about Gunbot’s pairs, such as strategies used, bought price, sold price, price trend, among others

### How to manually edit the files ###

A - Linux
In Files explorer, go to *+Other locations* and SFTP to your VPS.
When connected, it's useful to right click on it and *Add it as a Bookmark* so you can easily go back to the server in case of needing to edit some file.

B- Windows
The faster way to edit files in a Linux VPS using a windows computer is with the combo [WinSCP](https://winscp.net/eng/download.php) + [Sublime Text 3](https://www.sublimetext.com/3). After installing both, it’s time to configure WinSCP:

![winscp1.png](https://bitbucket.org/repo/da6GyXo/images/2665488283-winscp1.png)

Fill in Host Name with your VPS IP; your User name and your password. Then press save and choose a name for your profile to easy login in a future time.
Once connected, *you might want to edit ALLPAIRS-params.js in your Gunbot folder*, for example. In this case, go to desired folder, locate the file and double click on it for the default WinSCP text editor or right click and choose open with Sublime Text.

![winscp2.png](https://bitbucket.org/repo/da6GyXo/images/1890321155-winscp2.png)


## How to properly exit Cripto’s Gunbot System ##
When you are ready to *leave the system*, press **Ctrl+A d** to ‘dettach’ the system and go back to server’s main screen. Then, type **logout** or **exit** to *completely quit the application*. This way, the system will be running in background even when away from the computer. In case of a crash, the system possesses an auto save and auto restore feature. All the windows will be restored, and you only need to restart pairs and applications.

## What to do in case of errors ##
Sometimes server needs to be rebooted. When rebuilding the Console, it might happen to lost the shortcuts. I’m working in a fix for this, but right now the procedure is:
In main VPS window, type **crash**.
After that, type tmux. System will open and rebuild itself to the last saved layout.
In each pane: **Ctrl+A ALT+Q** then restart your pair with it’s command

### Useful commands: ###

**gcd** | moves to Gunbot folder
-- | --
**clear** | clears the screen
**cd ..** | returns to previous directory
**ctrl+c** | stops a process
**source ~/.profile** | reloads shortcuts





2017 | Criptonauta | Hacking a better world | Thank you Gunthar for the adventure!